import os
import subprocess
import sys

from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

sep = '/' if (os.name == "posix") else '\\'
imagesPath = os.getcwd() + sep + "images" + sep


def clear(layout):
    if layout is not None:
        while layout.count():
            item = layout.takeAt(0)
            widget = item.widget()
            if widget is not None:
                widget.setParent(None)
            else:
                clear(item.layout())


def openFile(fileName):
    if os.name == "posix":
        os.system("gedit " + fileName)
    else:
        subprocess.Popen(r"notepad.exe " + fileName)


class DirLabel(QLabel):
    clicked = pyqtSignal(str)

    def __init__(self, files, dirName):
        super().__init__()
        self.dirName = dirName
        self.grid = files

    def mousePressEvent(self, ev):
        os.chdir(self.dirName)
        self.grid.repaint()


class FileLabel(QLabel):
    clicked = pyqtSignal(str)

    def __init__(self, fileName):
        super().__init__()
        self.fileName = fileName

    def mousePressEvent(self, ev):
        openFile(self.fileName)


class DirCard(QVBoxLayout):
    def __init__(self, name: str, files):
        super().__init__()

        self.picLabel = DirLabel(files, name)
        self.picLabel.setPixmap(QPixmap(imagesPath + "dir.png"))
        self.picLabel.setAlignment(Qt.AlignCenter)
        self.addWidget(self.picLabel)

        self.nameLabel = QLabel(name)
        self.nameLabel.setStyleSheet("color: #f5f5f5;")
        self.nameLabel.setAlignment(Qt.AlignCenter)
        self.addWidget(self.nameLabel)


class FileCard(QVBoxLayout):
    def __init__(self, name: str):
        super().__init__()

        self.picLabel = FileLabel(name)
        self.picLabel.setPixmap(QPixmap(imagesPath + "file.png"))
        self.picLabel.setAlignment(Qt.AlignCenter)
        self.addWidget(self.picLabel)

        self.nameLabel = QLabel(name)
        self.nameLabel.setStyleSheet("color: #f5f5f5;")
        self.nameLabel.setAlignment(Qt.AlignCenter)
        self.addWidget(self.nameLabel)


class Empty(QVBoxLayout):
    def __init__(self):
        super().__init__()
        self.label2 = QLabel(" ")
        self.label2.setAlignment(Qt.AlignCenter)
        self.addWidget(self.label2)


class FileLayout(QGridLayout):
    def __init__(self):
        super().__init__()
        self.files = []
        self.repaint()

    def updateFiles(self):
        self.files = os.listdir()
        self.files.sort()
        self.files.reverse()

    def repaint(self):
        self.updateFiles()
        clear(self)
        self.addLayout(DirCard("..", self), 0, 0)
        for i in range(3):
            for j in range(6):
                if i == 0 and j == 0:
                    pass
                elif len(self.files) != 0:
                    if os.path.isdir(self.files[-1]):
                        self.addLayout(DirCard(self.files.pop(), self), i, j)
                    else:
                        self.addLayout(FileCard(self.files.pop()), i, j)
                else:
                    self.addLayout(Empty(), i, j)


class MySideBar(QVBoxLayout):


    def __init__(self, files):
        super().__init__()

        self.files = files

        self.widget = QWidget()
        self.widget.setStyleSheet("color: #f5f5f5; background-color: #1e1e2e;")
        self.inputDialog = QInputDialog()

        newFileButton = QPushButton("Создать файл")
        newDirButton = QPushButton("Создать папку")
        removeButton = QPushButton("Удалить")
        exitButton = QPushButton("Выйти")

        newFileButton.setFixedWidth(120)
        newDirButton.setFixedWidth(120)
        removeButton.setFixedWidth(120)
        exitButton.setFixedWidth(120)

        newFileButton.setStyleSheet("color: #f5f5f5;")
        newDirButton.setStyleSheet("color: #f5f5f5;")
        removeButton.setStyleSheet("color: #f5f5f5;")
        exitButton.setStyleSheet("color: #f5f5f5;")

        newFileButton.clicked.connect(self.newFile)
        newDirButton.clicked.connect(self.newDir)
        removeButton.clicked.connect(self.removeFile)
        exitButton.clicked.connect(exit)

        self.setAlignment(Qt.AlignTop)
        self.addLayout(Empty())
        self.addWidget(newFileButton)
        self.addLayout(Empty())
        self.addWidget(newDirButton)
        self.addLayout(Empty())
        self.addWidget(removeButton)
        self.addLayout(Empty())
        self.addWidget(exitButton)

    def newFile(self):
        fileName, a = self.inputDialog.getText(self.widget, "Создание файла", "Введите название файла:")
        open(fileName, "w")
        self.files.repaint()

    def newDir(self):
        dirName, a = self.inputDialog.getText(self.widget, "Создание папки", "Введите название папки:")
        os.mkdir(dirName)
        self.files.repaint()

    def removeFile(self):
        fileName, a = self.inputDialog.getText(self.widget, "Удаление", "Введите название файла или папки:")
        try: os.remove(fileName)
        except: pass
        try: os.rmdir(fileName)
        except:
            if os.path.isdir(fileName):
                QMessageBox.warning(self.widget,"Внимание!", "Папка не является пустой.")
            else: pass
        self.files.repaint()


class MainWindow(QMainWindow):

    def __init__(self):
        super().__init__()
        self.setStyleSheet("background-color: #1e1e2e;")
        self.setWindowTitle("My App")
        self.setFixedWidth(1000)
        self.setFixedHeight(600)

        mainLayout = QHBoxLayout()
        fileLayout = FileLayout()
        mainLayout.addLayout(MySideBar(fileLayout))
        mainLayout.addLayout(fileLayout)

        widget = QWidget()
        widget.setLayout(mainLayout)
        self.setCentralWidget(widget)


app = QApplication(sys.argv)

window = MainWindow()
window.show()

app.exec()
